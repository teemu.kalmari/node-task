const express = require("express")
const app = express();
const PORT = 3000;


// build small REST API with Express

console.log("Server-side program starting...")

app.get('/', (req,res) => {
    res.send("Hello world");
});

/**
 * this arrow ads two numbers together
 * @param {number} a first number
 * @param {number} b second number
 * @returns {number}
 */
const add = (a, b) => {
     a = parseInt(a)
     b = parseInt(b)
     

    const sum = a + b
    return sum;
};

// Adding endpoint: http://localhost:3000/add/?a=value&b=value
app.get('/add', (req, res) => {
    console.log(req.query);
    const sum = add(req.query.a, req.query.b);
    res.send(sum.toString());
})
app.listen(PORT, () => console.log(`Server listening http://localhost:${PORT}`));